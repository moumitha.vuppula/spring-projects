package com.example.myapp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/account")
@RestController
public class AccountController {
	
	@Autowired
	AccountService accountService;
	@GetMapping
	List<Account> getAccount() {
		accountService.getAccount();
		System.out.println("called");
		return accountService.getAccount();
	}
	@GetMapping("/{id}")
	void getAccount(@PathVariable Integer id) {
		System.out.println("called.." +id);
	}
	@PostMapping
	private String saveAccount(@RequestBody Account account) {
		accountService.saveAcc(account);
		System.out.println("Account Numbet: " +account.getAccno());
		System.out.println("Account Type: " +account.getAcctype());
		System.out.println("Account Balance: " +account.getBalance());
		System.out.println("Account Status: " +account.getStatus());
		System.out.println("Account Created Date: " +account.getCreated_date());
		return "post called";
	}
	@PutMapping
	 String handlePut() {
		System.out.println("Put");
		return "put method called";

	}

	

}


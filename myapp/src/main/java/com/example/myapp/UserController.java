package com.example.myapp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/user")
@RestController
public class UserController {
	
	@Autowired
	UserService userService;
	@GetMapping
	List<User> getUsers() {
		userService.getUsers();
		System.out.println("called");
		return userService.getUsers();
	}
	@GetMapping("/{id}")
	void getUser(@PathVariable Integer id) {
		System.out.println("called.." +id);
	}
	@PostMapping
	private String saveUser(@RequestBody User user) {
		userService.save(user);
		System.out.println("User: " +user.getName());
		System.out.println("Age: " +user.getAge());
		return "post called";
	}
	@PutMapping
	 String handlePut() {
		System.out.println("Put");
		return "put method called";

	}

}
